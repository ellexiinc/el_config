#!/usr/bin/env python
# -*- coding: utf-8 -*-

from el_config import ConfigBase


def _pre_load(config):
    config.test = '123'
    return config

_valid_schema = {
    'test':
    {
        'type': 'integer',
        'coerce': int
    }
}

_cb = ConfigBase(pre_load=_pre_load, valid_schema=_valid_schema, freeze=False)
_cb.load()
config = _cb.config

print(config)

config.test = 333
print(config)