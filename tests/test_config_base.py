# -*- coding: utf-8 -*-

import unittest

import pytest

from el_logging import logger
from el_config import ConfigBase


class TestElConfigBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("Starting 'el_config' unittest...\n")

    @classmethod
    def tearDownClass(cls):
        logger.success("Successfully tested 'el_config'.")

    def test_init(self):
        logger.info("Testing initialization of 'el_config' modules...")
        self.assertIsNotNone(ConfigBase)
        logger.success('Done.\n')


@pytest.mark.parametrize('configs_dir, required_envs, pre_load, valid_schema, expected', [
    ('configs', [], lambda config: config, {}, True)
])
def test_config_base(configs_dir, required_envs, pre_load, valid_schema, expected):
    _cb = ConfigBase(configs_dir, required_envs, pre_load, valid_schema)
    _cb.load()

    assert (_cb.config is not None) == expected
